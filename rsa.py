"""
A very basic implementatoin of 'old RSA'

WARNING: This algorithm is old and unsecure, DO NOT USE FOR SENSITIVE DATA.
"""
from math import gcd
from tools import _find_prime, _mulinv

def rsa_gen_keys():
    """Generates public and private keys"""

    # generates two 1024-digit long primes
    prime_1 = _find_prime(pow(10, 1024), pow(10, 1025)-1)
    prime_2 = _find_prime(pow(10, 1024), pow(10, 1025)-1, prime_1)

    # computes modulus
    modulus = prime_1 * prime_2

    # computes totient
    totient = (prime_1 - 1) * (prime_2 - 1)

    # public key
    pub_key = 65537
    while gcd(pub_key, totient) != 1:
        pub_key = _find_prime(3, totient-1)
    pub_string = f"PUBLIC KEY: ({pub_key}, {modulus})\n"

    # private key
    priv_key = _mulinv(pub_key, totient)
    priv_string = f"PRIVATE KEY: ({priv_key}, {modulus})"

    return pub_string + priv_string

def encrypt_msg(msg, public_key, modulus):
    """Encrypts message using RSA public key"""
    return pow(msg, public_key, modulus)

def decrypt_msg(encrypted_msg, private_key, modulus):
    """Decrypts encrypted message using RSA private key"""
    return pow(encrypted_msg, private_key, modulus)
