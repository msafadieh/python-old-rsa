"""
A few algorithms used in the RSA implementation.
"""
from random import randint
from _primefac import isprime

def _find_prime(min_val, max_val, other=None):
    """
    Finds prime number in the interval [min_val, max_val] (not
    including the optional parameter "other") if there exists one.
    """
    if min_val == max_val:
        raise ValueError("No prime values in interval.")

    original = randint(min_val, max_val)
    if original % 2 == 0: original += 1
    rand_val = original

    while (not isprime(rand_val)) or (rand_val == other):
        rand_val += 2
        if rand_val > max_val:
            rand_val = _find_prime(min_val, original, other)

    return rand_val

def __xgcd(b, a):
    """"
    Extended GCD implementation.
    
    Source: https://bit.ly/2J9CtZu
    """
    x0, x1, y0, y1 = 1, 0, 0, 1
    while a != 0:
        q, b, a = b // a, a, b % a
        x0, x1 = x1, x0 - q * x1
        y0, y1 = y1, y0 - q * y1
    return  b, x0, y0

def _mulinv(first_val, mod):
    """
    Modular inverse calculator.

    Same source as xgcd.
    """
    the_gcd, val, _ = __xgcd(first_val, mod)
    if the_gcd == 1:
        return val % mod
    raise ValueError("GCD of inputs is not 1.")
