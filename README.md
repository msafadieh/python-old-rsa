# python-old-rsa

## Disclaimer

This is an implementation of the **old** RSA and should not be used to encrypt sensitive information. There are much better and more secure tools out there.

## What is this?

This is a (very basic) implementation of RSA in Python. I followed [this](http://doctrina.org/How-RSA-Works-With-Examples.html) guide while building this.

## License

Code licensed under [Apache 2.0](LICENSE)